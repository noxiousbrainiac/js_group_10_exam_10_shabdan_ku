const express = require('express');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        if (req.query.news_id) {
            const [comment] = await mysqlDb.getConnection().query(
                `SELECT * FROM ?? where news_id = ?`,
                ['comments', req.query.news_id]);
            return res.send(comment);
        }

        const [comments] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['comments']);
        res.send(comments);
    } catch (e) {
        res.status(404).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        if (req.body.news_id) {
            if (!req.body.comment) {
                return res.status(400).send({error: 'Data not valid'});
            }

            const [findNewsId] = await mysqlDb.getConnection().query(
                `SELECT * FROM ?? where id = ?`,
                ['news', req.body.news_id]
            );

            if (!findNewsId || findNewsId.length === 0) {
                return res.status(404).send({error: 'No news id'});
            }

            const comment = {
                news_id: req.body.news_id,
                author: req.body.author,
                comment: req.body.comment
            }

            const newComment = await mysqlDb.getConnection().query(
                'INSERT INTO ?? (news_id, author, comment) values (?, ?, ?)',
                ['comments', comment.news_id, comment.author === "" ? "Anonymous" : comment.author, comment.comment]
            );

            return res.send({
                ...comment,
                id: newComment[0].insertId
            });
        }
    } catch (e) {
        res.status(404).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id  = ?',
            ['comments', req.params.id]
        );

        res.send({message: `Deleted successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(404).send(e);
    }
});

module.exports = router;


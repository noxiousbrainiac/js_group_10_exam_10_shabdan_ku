const express = require('express');
const mysqlDb = require('../mysqlDb');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const [news] = await mysqlDb.getConnection().query('SELECT * FROM ??', ['news']);
        return res.send(news);
    } catch (e) {
        res.status(404).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [singleNews] = await mysqlDb.getConnection().query(
            `SELECT * FROM ?? where id = ?`,
            ['news', req.params.id]);

        if (!singleNews[0]) {
            return res.status(404).send({error: 'No data here'});
        }

        return res.send(singleNews[0]);
    } catch (e) {
        res.status(404).send(e);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        if (!req.body.title|| !req.body.body) {
            return res.status(400).send({error: 'Data not valid'});
        }

        const news = {
            title: req.body.title,
            body: req.body.body
        }

        if (req.file) {
            news.image = req.file.filename;
        }

        const newComment = await mysqlDb.getConnection().query(
            'INSERT INTO ?? (title, body, image) values (?, ?, ?)',
            ['news', news.title, news.body, news.image]
        );

        return res.send({
            ...news,
            id: newComment[0].insertId
        });
    } catch (e) {
        res.status(404).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id  = ?',
            ['news', req.params.id]
        );

        return res.send({message: `Deleted successful, id= ${req.params.id}`});
    } catch (e) {
        res.status(404).send(e);
    }
});

module.exports = router;


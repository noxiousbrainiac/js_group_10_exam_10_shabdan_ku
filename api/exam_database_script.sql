DROP database if exists exam_database;
CREATE database if not exists exam_database;

use exam_database;

create table if not exists news (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    body longtext null,
    image varchar(255) null,
    datetime datetime default CURRENT_TIMESTAMP not null
);

create table comments (
    id int not null auto_increment primary key,
    news_id int not null,
    author varchar(255) null,
    comment text null,
    constraint news_comments_id_fk
        foreign key (news_id)
            references news (id)
            on update cascade
            on delete cascade
);

insert into news (title, body, image)
values ('First', 'some text here', null),
       ('Second', 'some text here', null);

insert into comments (news_id, author, comment)
values (1, 'Vova', 'Hello world!'),
       (2, 'Dima', 'Hello world!');

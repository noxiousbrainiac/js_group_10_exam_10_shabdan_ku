const express = require('express');
const cors = require('cors');
const mysqlDb = require('./mysqlDb');
const newsRoutes = require('./routes/newsRoutes');
const commentsRoutes = require('./routes/commentsRoutes');

const port = 8000;

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/news', newsRoutes);
app.use('/comments', commentsRoutes);

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => console.log(`Server started on ${port} port!`));

import React from 'react';
import CommentItem from "./CommentItem/CommentItem";

const CommentList = ({comments}) => {
    return (
        <>
            {
                comments.length !== 0
                    ? comments.map(com => (
                        <CommentItem key={com.id} comment={com.comment} id={com.id} author={com.author}/>
                    ))
                    : <p>No comments</p>
            }
        </>
    );
};

export default CommentList;
import React from 'react';
import {Button, Card, makeStyles} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {deleteComment, getComments} from "../../../store/actions/commentsActions";
import {useParams} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        display: "flex",
        padding: "5px",
        alignItems: "center",
        backgroundColor: "beige",
        marginBottom: "10px"
    },
    comment: {
        flexGrow: "1"
    }
})

const CommentItem = ({comment, id, author}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const params = useParams();

    const removeComment = async () => {
        await dispatch(deleteComment(id));
        await dispatch(getComments(params.id));
    }

    return (
        <Card className={classes.card}>
            <div className={classes.comment}>
                <b>{author}</b>
                <p>{comment}</p>
            </div>
            <Button
                type="button"
                variant="contained"
                color="secondary"
                size="small"
                onClick={removeComment}
            >
                Delete
            </Button>
        </Card>
    );
};

export default CommentItem;
import React from 'react';
import {makeStyles, Toolbar} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const useStyle = makeStyles({
    Toolbar: {
        display:"flex",
        background: "gray",
        justifyContent: "space-between"
    },
    title: {
        color: "beige",
        margin: "0 0 0 5px"
    },
    link: {
        textDecoration: "none"
    }
})
const AppToolbar = () => {
    const classes = useStyle();

    return (
        <Toolbar className={classes.Toolbar}>
            <NavLink to="/" className={classes.link}>
                <h3 className={classes.title}>News</h3>
            </NavLink>
        </Toolbar>
    );
};

export default AppToolbar;
import React, {useState} from 'react';
import {Button, makeStyles, TextField} from "@material-ui/core";
import FileInput from "../FileInput/FileInput";

const useStyles = makeStyles({
    inputs: {
        marginBottom: "10px"
    },
    formContainer: {
        display: "flex",
        flexDirection: "column",
        width: "500px"
    },
    form: {
        marginBottom: "20px"
    }
});

const AppForm = ({send}) => {
    const classes = useStyles();

    const [input, setInput] = useState({
        title: "",
        body: "",
        image: null
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(input).forEach(key => {
            formData.append(key, input[key]);
        });

        if (input.body.length !== 0 && input.title.length !== 0) {
            send(formData);
        }
    };

    const inputHandle = e => {
        const name = e.target.name;
        const value = e.target.value;
        setInput(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setInput(prevState => {
            return {...prevState, [name]: file}
        });
    };

    return (
        <form
            className={classes.form}
            onSubmit={submitFormHandler}
        >
            <div className={classes.formContainer}>
                <TextField
                    className={classes.inputs}
                    name="title"
                    label="Title"
                    value={input.title}
                    onChange={inputHandle}
                />
                <TextField
                    className={classes.inputs}
                    name="body"
                    label="Text"
                    value={input.body}
                    onChange={inputHandle}
                />
                <FileInput
                    onChange={fileChangeHandler}
                    label="Image"
                    name="image"
                />
            </div>
            <Button variant="contained" color="primary" type="submit" style={{marginTop: "20px"}}>
                Send
            </Button>
        </form>
    );
};

export default AppForm;
import React from 'react';
import {Button, Card, makeStyles} from "@material-ui/core";
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getNews, removeNews} from "../../../store/actions/newsActions";
import {apiUrl} from "../../../constants";

const useStyles = makeStyles({
    img: {
        width: "100%"
    },
    imgDiv: {
        width: "150px",
        height: "100%",
        margin: "0 10px 0 0"
    },
    card: {
        display: "flex",
        padding: "10px",
        minHeight: "100px",
        alignItems: "center",
        marginBottom: "20px",
        backgroundColor: "beige"
    },
    cardContent: {
        display: "flex",
        justifyContent: "space-evenly",
        flexGrow: "1"
    },
    btn: {
        marginLeft: "10px"
    }
});

const NewsItem = ({image, datetime, title, id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const noImg = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png';
    const history = useHistory();

    const remove = async () => {
        await dispatch(removeNews(id));
        await dispatch(getNews());
    }

    const read = () => {
        history.push(`news/${id}`);
    }

    return (
        <Card className={classes.card}>
            <div className={classes.imgDiv}>
                <img
                    src={image ? `${apiUrl}/${image}` : noImg}
                    alt="news img"
                    className={classes.img}/>
            </div>
            <div style={{flexGrow: "1"}}>
                <h4>{title}</h4>
                <div className={classes.cardContent}>
                    <p style={{flexGrow: "1"}}>{datetime}</p>
                    <div style={{display: "flex"}}>
                        <Button
                            className={classes.btn}
                            variant="contained"
                            color="primary"
                            size="small"
                            onClick={read}
                        >
                            Read Full Post >>
                        </Button>
                        <Button
                            className={classes.btn}
                            type="button"
                            variant="contained"
                            color="secondary"
                            size="small"
                            onClick={remove}
                        >
                            Delete
                        </Button>
                    </div>
                </div>
            </div>
        </Card>
    );
};

export default NewsItem;
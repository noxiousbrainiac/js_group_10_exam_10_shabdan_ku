import React from 'react';
import NewsItem from "./NewsItem/NewsItem";

const NewsList = ({news}) => {

    return (
        <div style={{paddingTop: "20px"}}>
            {news.length !== 0 ? news.map(item => (
                <NewsItem
                    key={item.id}
                    image={item.image}
                    title={item.title}
                    datetime={item.datetime}
                    id={item.id}
                />
            )) : <h3>No news</h3>}
        </div>
    );
};

export default NewsList;
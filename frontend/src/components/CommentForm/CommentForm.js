import React, {useState} from 'react';
import {Button, makeStyles, TextField} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createComment, getComments} from "../../store/actions/commentsActions";

const useStyles = makeStyles({
    inputS: {
        marginBottom: "10px"
    },
    formS: {
        display: "flex",
        flexDirection: "column",
        alignItems: "start"
    }
})

const CommentForm = ({id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [input, setInput] = useState({
        author: "",
        comment: ""
    });

    const inputHandler = (e) => {
        const {name, value} = e.target;
        setInput(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const add = async (e) => {
        e.preventDefault();
        if (input.comment.length !== 0) {
            await dispatch(createComment({
                ...input,
                news_id: id
            }));
        }
        await dispatch(getComments(id));
    }

    return (
        <form className={classes.formS} onSubmit={e => add(e)}>
            <h3>Add comment</h3>
            <TextField
                label="Name"
                variant="outlined"
                size="small"
                className={classes.inputS}
                onChange={inputHandler}
                value={input.author}
                name="author"
            />
            <TextField
                label="Comment"
                variant="outlined"
                size="small"
                multiline
                className={classes.inputS}
                onChange={inputHandler}
                value={input.comment}
                name="comment"
            />
            <Button
                variant="contained"
                color="primary"
                size="small"
                type="submit"
            >
                Add
            </Button>
        </form>
    );
};

export default CommentForm;
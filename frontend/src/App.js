import React from 'react';
import NewsContainer from "./containers/NewsContainer/NewsContainer";
import {Route, Switch} from "react-router-dom";
import AppToolbar from "./components/Navigation/AppToolbar/AppToolbar";
import ArticleContainer from "./containers/ArticleContainer/ArticleContainer";
import NewsFormContainer from "./containers/NewsFormContainer/NewsFormContainer";

const App = () => {
    return (
        <>
            <AppToolbar/>
            <Switch>
                <Route exact path="/" component={NewsContainer}/>
                <Route path='/addNewArticle' component={NewsFormContainer}/>
                <Route path="/news/:id" component={ArticleContainer}/>
            </Switch>
        </>
    );
};

export default App;
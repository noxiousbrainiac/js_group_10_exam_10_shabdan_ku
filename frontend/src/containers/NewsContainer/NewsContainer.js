import React, {useEffect} from 'react';
import {Button, Container, makeStyles} from "@material-ui/core";
import NewsList from "../../components/NewsList/NewsList";
import {useDispatch, useSelector} from "react-redux";
import {getNews} from "../../store/actions/newsActions";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles({
    newsHeader: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
    }
})

const NewsContainer = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const classes = useStyles();
    const news = useSelector(state => state.news.news);

    const toAddPage = () => {
        history.push('/addNewArticle');
    }

    useEffect(() => {
        dispatch(getNews());
    }, [dispatch]);

    return (
        <Container>
            <div className={classes.newsHeader}>
                <h4>News</h4>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={toAddPage}
                >
                    Add new article
                </Button>
            </div>
            <NewsList news={news}/>
        </Container>
    );
};

export default NewsContainer;
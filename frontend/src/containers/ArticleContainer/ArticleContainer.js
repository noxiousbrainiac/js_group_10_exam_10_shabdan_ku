import React, {useEffect} from 'react';
import {Container} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getArticle} from "../../store/actions/newsActions";
import {useParams} from "react-router-dom";
import {getComments} from "../../store/actions/commentsActions";
import CommentList from "../../components/CommentList/CommentList";
import CommentForm from "../../components/CommentForm/CommentForm";

const ArticleContainer = () => {
    const dispatch = useDispatch();
    const params = useParams();
    const {comments, article} = useSelector(state => ({
        comments: state.comments.comments,
        article: state.news.article
    }));

    useEffect(() => {
        dispatch(getArticle(params.id));
        dispatch(getComments(params.id));
    }, [dispatch, params.id]);

    return (
        <Container>
            <h2>{article.title}</h2>
            <b>{article.datetime}</b>
            <p>{article.body}</p>
            <h3>Comments:</h3>
            <CommentList comments={comments}/>
            <CommentForm id={params.id}/>
        </Container>
    );
};

export default ArticleContainer;
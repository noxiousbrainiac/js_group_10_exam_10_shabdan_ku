import React from 'react';
import {Container} from "@material-ui/core";
import AppForm from "../../components/UI/AppForm/AppForm";
import {useDispatch} from "react-redux";
import {postArticle} from "../../store/actions/newsActions";
import {useHistory} from "react-router-dom";

const NewsFormContainer = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const send = async (article) => {
        await dispatch(postArticle(article));
        history.push('/');
    }

    return (
        <Container>
            <AppForm send={send}/>
        </Container>
    );
};

export default NewsFormContainer;
import axiosApi from "../../axiosApi";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const DELETE_COMMENT_REQUEST = "DELETE_COMMENT_REQUEST";
export const DELETE_COMMENT_SUCCESS = "DELETE_COMMENT_SUCCESS";
export const DELETE_COMMENT_FAILURE = "DELETE_COMMENT_FAILURE";

export const POST_COMMENT_REQUEST = "POST_COMMENT_REQUEST";
export const POST_COMMENT_SUCCESS = "POST_COMMENT_SUCCESS";
export const POST_COMMENT_FAILURE = "POST_COMMENT_FAILURE";

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = (data) => ({type: FETCH_COMMENTS_SUCCESS, payload: data});
export const fetchCommentsFailure = (error) => ({type: FETCH_COMMENTS_FAILURE, payload: error});

export const deleteCommentRequest = () => ({type: DELETE_COMMENT_REQUEST});
export const deleteCommentSuccess = () => ({type: DELETE_COMMENT_SUCCESS});
export const deleteCommentFailure = (error) => ({type: DELETE_COMMENT_FAILURE, payload: error});

export const postCommentRequest = () => ({type: POST_COMMENT_REQUEST});
export const postCommentSuccess = () => ({type: POST_COMMENT_SUCCESS});
export const postCommentFailure = (error) => ({type: POST_COMMENT_FAILURE, payload: error})

export const getComments = (id) => async (dispatch) => {
    try {
        dispatch(fetchCommentsRequest());
        const {data} = await axiosApi.get(`/comments/?news_id=${id}`);
        dispatch(fetchCommentsSuccess(data));
    } catch (e) {
        dispatch(fetchCommentsFailure(e));
    }
}

export const deleteComment = (id) => async (dispatch) => {
    try {
        dispatch(deleteCommentRequest());
        await axiosApi.delete(`/comments/${id}`)
        dispatch(deleteCommentSuccess());
    } catch (e) {
        dispatch(deleteCommentFailure(e));
    }
}

export const createComment = (comment) => async (dispatch) => {
    try {
        dispatch(postCommentRequest());
        await axiosApi.post(`/comments`, comment);
        dispatch(postCommentSuccess());
    } catch (e) {
        dispatch(postCommentFailure(e));
    }
}
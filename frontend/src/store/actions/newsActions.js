import axiosApi from "../../axiosApi";

export const FETCH_NEWS_REQUEST = "FETCH_NEWS_REQUEST";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_FAILURE = "FETCH_NEWS_FAILURE";

export const DELETE_NEWS_REQUEST = "DELETE_NEWS_REQUEST";
export const DELETE_NEWS_SUCCESS = "DELETE_NEWS_SUCCESS";
export const DELETE_NEWS_FAILURE = "DELETE_NEWS_FAILURE";

export const FETCH_SINGLE_NEWS_REQUEST = "FETCH_SINGLE_NEWS_REQUEST";
export const FETCH_SINGLE_NEWS_SUCCESS = "FETCH_SINGLE_NEWS_SUCCESS";
export const FETCH_SINGLE_NEWS_FAILURE = "FETCH_SINGLE_NEWS_FAILURE";

export const POST_ARTICLE_REQUEST = "POST_ARTICLE_REQUEST";
export const POST_ARTICLE_SUCCESS = "POST_ARTICLE_SUCCESS";
export const POST_ARTICLE_FAILURE = "POST_ARTICLE_FAILURE";

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = (news) => ({type: FETCH_NEWS_SUCCESS, payload: news});
export const fetchNewsFailure = (error) => ({type: FETCH_NEWS_FAILURE, payloadL: error});

export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSuccess = () => ({type: DELETE_NEWS_SUCCESS});
export const deleteNewsFailure = (error) => ({type: DELETE_NEWS_FAILURE, payload: error});

export const fetchSingleNewsRequest = () => ({type: FETCH_SINGLE_NEWS_REQUEST});
export const fetchSingleNewsSuccess = (article) => ({type: FETCH_SINGLE_NEWS_SUCCESS, payload: article});
export const fetchSingleNewsFailure = (error) => ({type: FETCH_SINGLE_NEWS_FAILURE, payload: error});

export const postArticleRequest = () => ({type: POST_ARTICLE_REQUEST});
export const postArticleSuccess = () => ({type: POST_ARTICLE_SUCCESS});
export const postArticleFailure = (error) => ({type: POST_ARTICLE_FAILURE, payload: error});

export const getNews = () => async (dispatch) => {
    try {
        dispatch(fetchNewsRequest());
        const {data} = await axiosApi.get('/news');
        dispatch(fetchNewsSuccess(data));
    } catch (e) {
        dispatch(fetchNewsFailure(e));
    }
}

export const getArticle = (id) => async (dispatch) => {
    try {
        dispatch(fetchSingleNewsRequest());
        const {data} = await axiosApi.get(`/news/${id}`);
        dispatch(fetchSingleNewsSuccess(data));
    } catch (e) {
        dispatch(fetchSingleNewsFailure(e));
    }
}

export const removeNews = (id) => async (dispatch) => {
    try {
        dispatch(deleteNewsSuccess());
        await axiosApi.delete(`/news/${id}`);
        dispatch(deleteNewsSuccess());
    } catch (e) {
        dispatch(deleteNewsFailure(e));
    }
}

export const postArticle = (article) => async (dispatch) => {
    try {
        dispatch(postArticleRequest());
        await axiosApi.post('/news', article);
        dispatch(postArticleSuccess());
    } catch (e) {
        dispatch(postArticleFailure(e));
    }
}

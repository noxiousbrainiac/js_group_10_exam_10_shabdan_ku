import {
    DELETE_NEWS_FAILURE,
    DELETE_NEWS_REQUEST,
    DELETE_NEWS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    FETCH_SINGLE_NEWS_FAILURE,
    FETCH_SINGLE_NEWS_REQUEST,
    FETCH_SINGLE_NEWS_SUCCESS, POST_ARTICLE_FAILURE,
    POST_ARTICLE_REQUEST, POST_ARTICLE_SUCCESS
} from "../actions/newsActions";


const initialState = {
    news: [],
    error: false,
    loading: false,
    article: {}
}

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, loading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, loading: false, news: action.payload};
        case FETCH_NEWS_FAILURE:
            return {...state, error: action.payload, loading: false};
        case DELETE_NEWS_REQUEST:
            return {...state, loading: true};
        case DELETE_NEWS_SUCCESS:
            return {...state, loading: false};
        case DELETE_NEWS_FAILURE:
            return {...state, error: action.payload, loading: false};
        case FETCH_SINGLE_NEWS_REQUEST:
            return {...state, loading: true};
        case FETCH_SINGLE_NEWS_SUCCESS:
            return {...state, loading: false, article: action.payload};
        case FETCH_SINGLE_NEWS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case POST_ARTICLE_REQUEST:
            return {...state, loading: true};
        case POST_ARTICLE_SUCCESS:
            return {...state, loading: false};
        case POST_ARTICLE_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}

export default newsReducer;
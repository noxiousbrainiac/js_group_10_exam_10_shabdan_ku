import {
    DELETE_COMMENT_FAILURE,
    DELETE_COMMENT_REQUEST, DELETE_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS, POST_COMMENT_FAILURE, POST_COMMENT_REQUEST, POST_COMMENT_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    comments: [],
    error: null,
    loading: false,

}

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, loading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.payload, loading: false};
        case FETCH_COMMENTS_FAILURE:
            return {...state, error: action.payload, loading: false};
        case DELETE_COMMENT_REQUEST:
            return {...state, loading: true};
        case DELETE_COMMENT_SUCCESS:
            return {...state, loading: false};
        case DELETE_COMMENT_FAILURE:
            return {...state, error: action.payload, loading: false};
        case POST_COMMENT_REQUEST:
            return {...state, loading: true};
        case POST_COMMENT_SUCCESS:
            return {...state, loading: false};
        case POST_COMMENT_FAILURE:
            return {...state, error: action.payload, loading: false};
        default:
            return state;
    }
}

export default commentsReducer;